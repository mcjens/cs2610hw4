from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.utils import timezone

def index(request):
    timezone.activate('America/Denver')
    time = timezone.localtime(value=None, timezone=None)
    return HttpResponse("Hello World! Thanks for visiting the page at %s " % time)
